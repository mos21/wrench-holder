include <peglib/defaults.scad>;
include <moslib/libchamfer.scad>;



closed_chamfered_tube(base_size * 2, base_size, base_size - (wall_thickness * 2), align = [0, 0], chamfer = wall_thickness / 4, cutout = false);

color("red") cube([base_size, base_size, base_size * 2]);
